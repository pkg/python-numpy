Source: python-numpy
Section: python
Priority: optional
Maintainer: Sandro Tosi <morph@debian.org>
Uploaders: Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>
Build-Depends: cython (>= 0.26-2.1),
               debhelper (>= 11),
               dh-python,
               gfortran (>= 4:4.2),
               libblas-dev [!arm !m68k],
               liblapack-dev [!arm !m68k],
               python2.7-dev (>= 2.7.14~rc1-1),
               python-all-dbg,
               python3-all-dbg,
               python-all-dev,
               python3-all-dev,
               python-docutils,
               python3-matplotlib,
               python-pytest,
               python3-pytest,
               python-setuptools,
               python3-setuptools,
               python3-sphinx,
               python-tz,
               python3-tz,
Standards-Version: 4.3.0
Vcs-Git: https://salsa.debian.org/python-team/modules/python-numpy.git
Vcs-Browser: https://salsa.debian.org/python-team/modules/python-numpy
Homepage: http://www.numpy.org/

Package: python-numpy
Architecture: any
Depends: ${misc:Depends}, ${python:Depends}, ${shlibs:Depends}, python-pkg-resources
Suggests: gcc (>= 4:4.6.1-5),
          gfortran,
          python-dev,
          python-pytest,
          python-numpy-dbg,
          python-numpy-doc
Provides: python-f2py, python-numpy-dev, ${numpy:Provides}, ${python:Provides}
Breaks: python-astropy (<= 2.0.9-1),
        python-pbcore (<< 1.6.5+dfsg-1),
        python-skimage (<< 0.14.1-3),
        python-tables (<< 3.4.4-2),
        python-theano (<< 1.0.3+dfsg-1),
Description: Numerical Python adds a fast array facility to the Python language
 Numpy contains a powerful N-dimensional array object, sophisticated
 (broadcasting) functions, tools for integrating C/C++ and Fortran
 code, and useful linear algebra, Fourier transform, and random number
 capabilities.
 .
 Numpy replaces the python-numeric and python-numarray modules which are
 now deprecated and shouldn't be used except to support older
 software.

Package: python-numpy-dbg
Section: debug
Architecture: any
Multi-Arch: same
Depends: python-dbg,
         python-numpy (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Breaks: python-numpy (<< 1:1.7.1-1)
Replaces: python-numpy (<< 1:1.7.1-1)
Description: Fast array facility to the Python language (debug extension)
 Numpy contains a powerful N-dimensional array object, sophisticated
 (broadcasting) functions, tools for integrating C/C++ and Fortran
 code, and useful linear algebra, Fourier transform, and random number
 capabilities.
 .
 Numpy replaces the python-numeric and python-numarray modules which
 are now deprecated and shouldn't be used except to support older
 software.
 .
 This package contains the extension built for the Python debug interpreter.

Package: python3-numpy
Architecture: any
Depends: ${misc:Depends}, ${python3:Depends}, ${shlibs:Depends}, python3-pkg-resources
Suggests: gcc (>= 4:4.6.1-5),
          gfortran,
          python-numpy-doc,
          python3-dev,
          python3-pytest,
          python3-numpy-dbg
Provides: python3-f2py,
          python3-numpy-dev,
          ${numpy3:Provides},
          ${python3:Provides}
Breaks: python3-aplpy (<< 2.0~rc2-1),
        python3-astropy (<< 3.1-1),
        python3-ccdproc (<< 1.3.0-5),
        python3-dask (<< 1.0.0+dfsg-2),
        python3-skimage (<< 0.14.1-3),
        python3-tables (<< 3.4.4-2),
        python3-theano (<< 1.0.3+dfsg-1),
Description: Fast array facility to the Python 3 language
 Numpy contains a powerful N-dimensional array object, sophisticated
 (broadcasting) functions, tools for integrating C/C++ and Fortran
 code, and useful linear algebra, Fourier transform, and random number
 capabilities.
 .
 Numpy replaces the python-numeric and python-numarray modules which are
 now deprecated and shouldn't be used except to support older
 software.
 .
 This package contains Numpy for Python 3.

Package: python3-numpy-dbg
Section: debug
Architecture: any
Multi-Arch: same
Depends: python3-dbg,
         python3-numpy (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Breaks: python3-numpy (<< 1:1.7.1-1)
Replaces: python3-numpy (<< 1:1.7.1-1)
Description: Fast array facility to the Python 3 language (debug extension)
 Numpy contains a powerful N-dimensional array object, sophisticated
 (broadcasting) functions, tools for integrating C/C++ and Fortran
 code, and useful linear algebra, Fourier transform, and random number
 capabilities.
 .
 Numpy replaces the python-numeric and python-numarray modules which
 are now deprecated and shouldn't be used except to support older
 software.
 .
 This package contains the extension built for the Python 3 debug interpreter.

Package: python-numpy-doc
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Section: doc
Architecture: all
Multi-Arch: foreign
Description: NumPy documentation
 Numpy contains a powerful N-dimensional array object, sophisticated
 (broadcasting) functions, tools for integrating C/C++ and Fortran
 code, and useful linear algebra, Fourier transform, and random number
 capabilities.
 .
 Numpy replaces the python-numeric and python-numarray modules which
 are now deprecated and shouldn't be used except to support older
 software.
 .
 This package contains documentation for Numpy and f2py.
